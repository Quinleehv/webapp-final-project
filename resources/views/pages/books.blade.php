@extends('layouts.app')

@section('content')
    <div class="container">
        <div>
            <a href="book/create"><button class="btn btn-primary col-2">Add a book</button></a>
        </div>
        <table class="col-12" style="text-align: center; margin-top: 20px;">
            <thead style="background-color: black; color: white; opacity: 0.7;">
                <tr>
                    <td>Book Number</td>
                    <td>Title</td>
                    <td>Author</td>
                    <td>Description</td>
                    <td>Date Publish</td>
                    <td>Action</td>
                    
                </tr>
            </thead>
                @if(count($books) > 0)
                    @foreach ($books as $book)
                        <tr>
                            <td>{{$book->book_id}}</td>
                            <td>{{$book->title}}</td>
                            <td>{{$book->author}}</td>
                            <td>{{$book->description}}</td>
                            <td>{{$book->date_publish}}</td>
                           
                            
                            <td>
                            <a  href="/books/{{$book->id}}/edit"><button class="btn btn-info">edit</button></a>
                            {!! Form::open(['action' => ['BooksController@destroy', $book->id], 'method' => 'POST']) !!}
                                {{Form::hidden('_method', 'DELETE')}}
                                <button class="btn btn-danger">delete</button>
                            {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <p>No records found.</p>
                @endif
                
            <tbody>
            </tbody>
        </table>
    </div>
@endsection