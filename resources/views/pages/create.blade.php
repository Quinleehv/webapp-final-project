@extends('layouts.app')

@section('content')
    <div class="container">
        <div>
            <a href="/books"><button class="btn btn-danger">Go back</button></a>
        </div>
        {!! Form::open(['action' => 'BooksController@store', 'method' => 'POST', 'style' => 'margin-top: 20px;']) !!}
            <div class="form-group row">
                <div class="col-2">
                <label>Book ID:</label>
                <input class="form-control" type="text" name="book_id">
                </div>
                <div class="col-10">
                <label>Title:</label>
                <input class="form-control" type="text" name="title">
                </div>
            </div>
            <div class="form-group">
                <label>Author:</label> 
                <input class="form-control" type="text" name="author">
            </div>
            <div class="form-group">
                <label>Description:</label>
                <input class="form-control" type="text" name="description">
            </div>
            <div class="form-group">
                <label>Date Publish:</label>
                <input class="form-control" type="text" name="date_publish">
            </div>
            <div class="form-group">
                <input class="btn btn-primary form-control" type="submit" value="Save">
            </div>
        {!! Form::close() !!}
    </div>
@endsection