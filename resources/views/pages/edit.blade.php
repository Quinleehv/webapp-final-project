@extends('layouts.app')

@section('content')
    <div class="container">
        <div>
            <a href="/books"><button class="btn btn-warning">Go back</button></a>
        </div>
        {!! Form::open(['action' => ['BooksController@update', $book->id], 'method' => 'POST', 'style' => 'margin-top: 20px;']) !!}
            {{Form::hidden('_method', 'PUT')}}
            <div class="form-group row">
                <div class="col-2">
                    <label>Book ID:</label>
                    <input type="text" value="{{$book->book_id}}" class="form-control" name="book_id">
                </div>
                <div class="col-10">
                    <label>Title</label>
                    <input type="text" value="{{$book->title}}" class="form-control" name="title">
                </div>
            </div>
            <div class="form-group">
                <label>Author:</label>
                <input type="text" value="{{$book->author}}" class="form-control" name="author">
            </div>
            <div class="form-group">
                <label>Description:</label>
                <input type="text" value="{{$book->description}}" class="form-control" name="description">
            </div>
            <div class="form-group">
                <label>Date Publish:</label>
                <input class="form-control" type="text" name="date_publish">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary form-control" value="Save">
            </div>
            {!! Form::close() !!}
    </div>
@endsection